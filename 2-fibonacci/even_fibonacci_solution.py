# Hier solltest du nicht lesen :-)
#
# Versuche doch besser, eine eigene Lösung zu finden!

def gerade_fibonacci_summe(element):
    a = 1
    b = 1
    c = 1
    ergebnis = []
    while a + b <= element:
        c = a + b
        if c % 2 == 0:
            ergebnis.append(c)
        a = b
        b = c
    return sum(ergebnis)
