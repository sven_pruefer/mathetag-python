# Gerade Fibonacci Zahlen
#
# Jeder neue Term der Fibonacci-Folge ist definiert als die Summe der beiden vorherigen Terme.
# Wenn man mit 1 und 2 beginnt, sind die ersten 10 Terme der Fibonacci-Folge also:
#
#  1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
#
# Finde die Summe der geraden Fibonacci-Zahlen, die kleiner als vier Millionen sind.

import even_fibonacci_solution   # Mit diesem Modul kann eure Lösung gecheckt werden


def deine_loesung(maximum: int) -> int:
    """
    Hier kommt euer Lösungsalgorithmus rein. 
    Euer Ergebnis soll dann letztendlich von dieser
    Funktion zurückgegeben werden!
    Die Variable 'maximum' bezeichnet den maximalen
    Wert, den ein Folgenglied der Fibonacci-Folge
    annehmen darf.
    """
    return 0


# Wenn ihr probieren wollt, ob euer Algorithmus funktioniert, könnt ihr für anzahl eine beliebige Zahl einsetzen.
# Um die Aufgabe korrekt zu lösen, sollte euer Ergebnis für anzahl = 4000000 mit der Musterlösung übereinstimmen.
anzahl = 4000000

# Hier wird eure Lösung gecheckt
dein_ergebnis = deine_loesung(anzahl)
musterloesung = even_fibonacci_solution.gerade_fibonacci_summe(anzahl)
if dein_ergebnis == musterloesung:
    print('Super, das ist ein korrektes Ergebnis :)')
else:
    print(f"""Schade, da scheint noch ein Fehler zu sein.

Dein Wert ist {dein_ergebnis}, der korrekte Wert wäre aber {musterloesung}.
Versuche doch noch den Fehler zu finden oder frag uns um Hilfe :)""")
