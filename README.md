# Fortgeschrittener Python-Programmierkurs Mathetag 2022

Project Euler Aufgaben für den Mathetag des Matheschülerzirkel Augsburg.

## Verwendung

In den Ordnern findest du die Aufgabenstellungen mitsamt einem
kleinen Testskript. Zum Lösen musst du die Funktion `deine_loesung`
implementieren und dann das Programm ausführen.

Öffne also z.B. `even_fibonacci.py` in einem Texteditor, programmiere
die Lösung und speichere dann die Datei. Danach kannst du eine Konsole
öffnen und dort z.B.

```shell
$ python even_fibonacci.py
```

ausführen. Das Programm sagt dir dann, ob du richtig lagst.

Viele Erfolg!

## Voraussetzungen

Man benötigt Python 3 und einen Texteditor.
