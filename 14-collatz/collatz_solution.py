# Hier solltest du nicht lesen :-)
#
# Versuche doch besser, eine eigene Lösung zu finden!

from typing import Dict, Tuple


def finde_laengste_collatz_kette_unter(max: int) -> Tuple[int, int]:
    """
    Berechnet die längste Collatz Folge von Startwerten kleiner gleich `max`
    :param max: Maximaler Startwert
    :return: Tupel von (Startwert, Länge der Collatz-Folge)
    """
    memoizer: Dict[int, int] = {}
    bisheriges_maximum: Tuple[int, int] = 0, 0
    for i in range(1, max + 1):
        laenge = berechne_collatz_laenge(memoizer, i)
        if laenge > bisheriges_maximum[1]:
            bisheriges_maximum = i, laenge
    # print(memoizer)
    return bisheriges_maximum


def berechne_collatz_laenge(memoizer: Dict[int, int], n: int) -> int:
    if n == 1:
        memoizer[1] = 1
        return 1
    elif n in memoizer.keys():
        return memoizer[n]
    elif n % 2 == 0:
        result = berechne_collatz_laenge(memoizer, n // 2) + 1
        memoizer[n] = result
        return result
    else:
        result = berechne_collatz_laenge(memoizer, 3 * n + 1) + 1
        memoizer[n] = result
        return result
