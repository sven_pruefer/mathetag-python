# Längste Collatz-Folge
#
# Die Collatz-Folge ist definiert wie folgt für natürliche Zahlen n:
#
#  n gerade -> n / 2
#  n ungerade -> 3 * n + 1
#
# Z.B. erhält man eine Folge der Länge 10 (bei 1 hört man auf), wenn man bei 13 startet:
#
# 13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1
#
# Frage: Welche Startzahl unter einer Million erzeugt die längste Collatz-Folge?
#
# Hinweis: Wenn die Folge gestartet ist, darf sie über eine Million gehen.

import collatz_solution


def deine_loesung(maximum: int) -> int:
    """
    Hier könnt ihr eure Lösung programmieren. Die Funktion sollte den Startwert mit
    der längsten Collatz-Folge von 1 bis `maximum` zurückgeben.
    """
    return 1


# Wenn ihr kleinere Zahlen zum Testen ausprobieren wollt,
# könnt ihr hier gerne den Wert für maximum verändern.
maximum: int = 1000000

dein_ergebnis = deine_loesung(maximum)
musterloesung = collatz_solution.finde_laengste_collatz_kette_unter(maximum)
if dein_ergebnis == musterloesung[0]:
    print("Sehr gut, du hast das Problem gelöst!")
else:
    print(f"""Dein Ergebnis stimmt leider nicht mit der Musterlösung überein.

Für ein Maximum von {maximum} hast du {dein_ergebnis} erhalten, aber die Musterlösung sagt {musterloesung[0]} mit einer Collatz-Länge von {musterloesung[1]}.""")
