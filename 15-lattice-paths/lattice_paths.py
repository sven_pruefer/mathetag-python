# Gitterwege
#
# Wenn man in einem 2x2-Gitter oben links startet und sich nur nach rechts oder nach unten bewegen kann,
# gibt es genau 6 Wege von links oben nach rechts unten:
#
# RRUU, RURU, RUUR, URRU, URUR, UURR
#
# Wie viele solche Wege gibt es in einem 20x20-Gitter?

import lattice_path_solution


def deine_loesung(groesse: int) -> int:
    """
    Hier könnt ihr eure Lösung programmieren. Die Funktion sollte
    die Anzahl der Wege auf einem Gitter der Größe 20x20 zurückgeben.
    """
    return 0


# Wenn ihr kleinere Zahlen zum Testen ausprobieren wollt,
# könnt ihr hier gerne den Wert für maximum verändern.
groesse: int = 20

dein_ergebnis = deine_loesung(groesse)
musterloesung = lattice_path_solution.anzahl_wege(groesse)
if dein_ergebnis == musterloesung:
    print("Sehr gut, du hast das Problem gelöst!")
else:
    print(f"""Dein Ergebnis stimmt leider nicht mit der Musterlösung überein.

Für die Größe {groesse} hast du {dein_ergebnis} Wege erhalten, aber die Musterlösung sagt {musterloesung} Wege.""")
