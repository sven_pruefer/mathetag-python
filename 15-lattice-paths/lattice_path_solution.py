# Hier solltest du nicht lesen :-)
#
# Versuche doch besser, eine eigene Lösung zu finden!

from typing import Tuple, List
import math


def anzahl_wege(groesse: int) -> int:
    return math.comb(2 * groesse, groesse)


def anzahl_wege_manuell(groesse: int) -> int:
    paths: List[Tuple[int, int]] = [(0, 0)]
    for i in range(2 * groesse):
        new_paths: List[Tuple[int, int]] = []
        for path in paths:
            if path[0] + 1 <= groesse:
                new_paths.append((path[0] + 1, path[1]))
            if path[1] + 1 <= groesse:
                new_paths.append((path[0], path[1] + 1))
        paths = new_paths
    return len([path for path in paths if path == (groesse, groesse)])
