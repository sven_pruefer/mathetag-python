# Hier solltest du nicht lesen :-)
#
# Versuche doch besser, eine eigene Lösung zu finden!

def pythagorean_triplet(summe):
    result = [((m**2 - n**2)*2*m*n*(m**2 + n**2))
              for n in range(1, 30)
              for m in range(n, 30)
              if 2 * m**2 + 2 * m * n == summe]
    return result[0]

