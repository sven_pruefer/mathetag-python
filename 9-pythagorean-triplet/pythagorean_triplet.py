# Spezielle Pythagoräische Tripel
#
# Ein Pythagoräisches Tripel ist ein Tripel von drei natürlichen Zahlen a < b < c, so dass
#
# a^2 + b^2 = c^2
#
# Zum Beispiel ist 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
#
# Es gibt genau ein Pythagoräisches Tripel mit a + b + c = 1000. Finde das Produkt a *  b * c.

import pythagorean_triplet_solution   # Mit diesem Modul kann eure Lösung gecheckt werden


def deine_loesung(summe: int) -> int:
    """
    Hier kommt dein Lösungsalgorithmus rein.
    Dein Ergebnis soll dann von dieser Funktion zurückgegeben werden!
    Die Variable 'summe' bezeichnet den Wert, den die Summe
    des Pythagoräischen Tripels haben soll.
    """
    return 0


# Wenn ihr probieren wollt, ob euer Algorithmus funktioniert, könnt ihr für die Variable 'summe' den Wert 12 benutzen.
# Um die Aufgabe korrekt zu lösen, sollte euer Ergebnis für summe = 1000 mit der Musterlösung übereinstimmen
summe = 1000
dein_ergebnis = deine_loesung(summe)
musterloesung = pythagorean_triplet_solution.pythagorean_triplet(summe)
if dein_ergebnis == musterloesung:
    print('Super das ist ein korrektes Ergebnis :)')
else:
    print(f"""Schade, da scheint noch ein Fehler zu sein.

Dein Wert ist {dein_ergebnis}, der korrekte Wert wäre aber {musterloesung}.
Versuche doch noch den Fehler zu finden oder frag uns um Hilfe :)""")
